from django.shortcuts import render
from django.conf import settings
from django.http import StreamingHttpResponse
from django.views.decorators import gzip
import cv2
import threading
# import asyncio
import numpy as np

# Create your views here.

from django.http import HttpResponse

camera_lock = threading.Lock()
global_frame=None

def home(request):
    return render(request, 'myproject/index.html')


def register_gate1(request):
    return render(request,'myproject/index1.html')

def register_gate2(request):
    return render(request,'myproject/index2.html')

def face_scan(request):
    return render(request, 'myproject/face.html')

def n_plate_scan(request):
    return render(request, 'myproject/n_plate.html')

def Video_feed(request):
    global global_frame
    video_capture=cv2.VideoCapture(0)
    face_cascade=cv2.CascadeClassifier(cv2.data.haarcascades+"haarcascade_frontalface_default.xml")
     
    while True:
        Success, frame=video_capture.read()
        
        if not Success:
            break
        
        gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        
        faces=face_cascade.detectMultiScale(frame,scaleFactor=1.1,minNeighbors=5,minSize=(30,30))
    
        for(x,y,h,w) in faces:
            face_detect=frame[x:x+w,y:y+h]
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
            cv2.putText(frame,"face_detected",(x,y-5),cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,1,(128,0,128),1)
            
            
        ret,buffer=cv2.imencode('.jpg',frame)
        frame=buffer.tobytes()
        
        
        # with camera_lock:
            # global_frame = frame_bytes
            
        yield(b'--frame\r\n'
              b'Content-Type: image/jpeg\r\n\r\n'+frame+b'\r\n')    
        
        
    video_capture.release()  
    return StreamingHttpResponse(Video_feed(),content_type="multipart/x-mixed-replace;boundary=frame")
    
    
            
            
    # return render(request, 'myproject/face.html')