from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('register_gate1/', views.register_gate1, name='register_gate1'),
    path('register_gate2/', views.register_gate2, name='register_gate2'),
    path('face_scan/', views.face_scan, name='face_scan'),
    path('n_plate_scan/', views.n_plate_scan, name='n_plate_scan'),
    path('Video_feed', views.Video_feed, name='Video_feed')
    
]
